﻿using BowlingGameScore.Bll.Classes;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingGameScore.Test
{
    [TestFixture]
    public class BallTest
    {
        #region [Fields]

        private BallThrow _ballThrow;
        #endregion

        #region [Methods]

        [SetUp]
        public void init()
        {
            _ballThrow = new BallThrow();
        }

        [Test]
        public void ScoreMustEqualAddedValue()
        {
            _ballThrow.AddScore(10);

            Assert.AreEqual(10, _ballThrow.score);
        }

        [Test]
        public void ScoreNotMoreThanTen()
        {
            _ballThrow.AddScore(11);

            Assert.AreEqual(10, _ballThrow.score);
        }

        [Test]
        public void ScoreNotLessThanZero()
        {
            _ballThrow.AddScore(-1);

            Assert.AreEqual(0, _ballThrow.score);
        }

        [Test]
        public void ScoreAddedNotChange()
        {
            _ballThrow.AddScore(5);
            _ballThrow.AddScore(1);

            Assert.AreEqual(5, _ballThrow.score);
        }
        #endregion
    }
}
