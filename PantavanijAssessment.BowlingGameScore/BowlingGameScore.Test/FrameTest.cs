﻿using BowlingGameScore.Bll.Classes;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingGameScore.Test
{
    [TestFixture]
    public class FrameTest
    {
        #region [Fields]

        private Frame _frame;
        #endregion

        #region [Methods]

        [SetUp]
        public void init()
        {
            _frame = new Frame();
        }

        [Test]
        public void KnockAllPinsOneBallIsStrike()
        {
            BallThrow ball = new BallThrow();
            ball.AddScore(10);
            _frame.AddBall(ball);
            Assert.AreEqual(Frame.FrameTypes.Strike, _frame.frameType);
        }

        [Test]
        public void FirstBallNotStrikeIsInProgress()
        {
            BallThrow ball = new BallThrow();
            ball.AddScore(6);
            _frame.AddBall(ball);
            Assert.AreEqual(Frame.FrameTypes.InProgress, _frame.frameType);
        }

        [Test]
        public void KnockAllPinsTwoBallIsSpare()
        {
            BallThrow ball1 = new BallThrow();
            ball1.AddScore(3);
            _frame.AddBall(ball1);

            BallThrow ball2 = new BallThrow();
            ball2.AddScore(7);
            _frame.AddBall(ball2);
            Assert.AreEqual(Frame.FrameTypes.Spare, _frame.frameType);
        }

        [Test]
        public void KnockNotAllPinsTwoBallIsOpenFrame()
        {
            BallThrow ball1 = new BallThrow();
            ball1.AddScore(1);
            _frame.AddBall(ball1);

            BallThrow ball2 = new BallThrow();
            ball2.AddScore(2);
            _frame.AddBall(ball2);
            Assert.AreEqual(Frame.FrameTypes.OpenFrame, _frame.frameType);
        }
        #endregion
    }
}
