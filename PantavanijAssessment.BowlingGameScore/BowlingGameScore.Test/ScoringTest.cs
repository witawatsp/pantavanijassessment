﻿using BowlingGameScore.Bll.Classes;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingGameScore.Test
{
    public class ScoringTest
    {
        #region [Field]

        private Scoring _scoring;
        #endregion

        [SetUp]
        public void init()
        {
            _scoring = new Scoring();
        }

        [Test]
        public void StrikeGetBonusTwoBall()
        {
            BallThrow ball1 = new BallThrow();
            ball1.AddScore(10);
            _scoring.AddBall(ball1);

            BallThrow ball2 = new BallThrow();
            ball2.AddScore(2);
            _scoring.AddBall(ball2);

            BallThrow ball3 = new BallThrow();
            ball3.AddScore(3);
            _scoring.AddBall(ball3);

            Assert.AreEqual(20, _scoring.GetTotalScroe());
        }

        [Test]
        public void SpareGetBonusOneBall()
        {
            BallThrow ball1 = new BallThrow();
            ball1.AddScore(7);
            _scoring.AddBall(ball1);

            BallThrow ball2 = new BallThrow();
            ball2.AddScore(3);
            _scoring.AddBall(ball2);

            BallThrow ball3 = new BallThrow();
            ball3.AddScore(6);
            _scoring.AddBall(ball3);

            BallThrow ball4 = new BallThrow();
            ball4.AddScore(2);
            _scoring.AddBall(ball4);

            Assert.AreEqual(24, _scoring.GetTotalScroe());
        }

        [Test]
        public void OpenFrameGetKnockDownPins()
        {
            BallThrow ball1 = new BallThrow();
            ball1.AddScore(7);
            _scoring.AddBall(ball1);

            BallThrow ball2 = new BallThrow();
            ball2.AddScore(2);
            _scoring.AddBall(ball2);

            Assert.AreEqual(9, _scoring.GetTotalScroe());
        }

        [Test]
        public void Test190score()
        {
            BallThrow ball1 = new BallThrow();
            ball1.AddScore(9);
            _scoring.AddBall(ball1);

            BallThrow ball2 = new BallThrow();
            ball2.AddScore(1);
            _scoring.AddBall(ball2);

            BallThrow ball3 = new BallThrow();
            ball3.AddScore(9);
            _scoring.AddBall(ball3);

            BallThrow ball4 = new BallThrow();
            ball4.AddScore(1);
            _scoring.AddBall(ball4);

            BallThrow ball5 = new BallThrow();
            ball5.AddScore(9);
            _scoring.AddBall(ball5);

            BallThrow ball6 = new BallThrow();
            ball6.AddScore(1);
            _scoring.AddBall(ball6);

            BallThrow ball7 = new BallThrow();
            ball7.AddScore(9);
            _scoring.AddBall(ball7);

            BallThrow ball8 = new BallThrow();
            ball8.AddScore(1);
            _scoring.AddBall(ball8);

            BallThrow ball9 = new BallThrow();
            ball9.AddScore(9);
            _scoring.AddBall(ball9);

            BallThrow ball10 = new BallThrow();
            ball10.AddScore(1);
            _scoring.AddBall(ball10);

            BallThrow ball11 = new BallThrow();
            ball11.AddScore(9);
            _scoring.AddBall(ball11);

            BallThrow ball12 = new BallThrow();
            ball12.AddScore(1);
            _scoring.AddBall(ball12);

            BallThrow ball13 = new BallThrow();
            ball13.AddScore(9);
            _scoring.AddBall(ball13);

            BallThrow ball14 = new BallThrow();
            ball14.AddScore(1);
            _scoring.AddBall(ball14);

            BallThrow ball15 = new BallThrow();
            ball15.AddScore(9);
            _scoring.AddBall(ball15);

            BallThrow ball16 = new BallThrow();
            ball16.AddScore(1);
            _scoring.AddBall(ball16);

            BallThrow ball17 = new BallThrow();
            ball17.AddScore(9);
            _scoring.AddBall(ball17);

            BallThrow ball18 = new BallThrow();
            ball18.AddScore(1);
            _scoring.AddBall(ball18);

            BallThrow ball19 = new BallThrow();
            ball19.AddScore(9);
            _scoring.AddBall(ball19);

            BallThrow ball20 = new BallThrow();
            ball20.AddScore(1);
            _scoring.AddBall(ball20);

            BallThrow ball21 = new BallThrow();
            ball21.AddScore(9);
            _scoring.AddBall(ball21);

            Assert.AreEqual(190, _scoring.GetTotalScroe());
        }

        [Test]
        public void Test300score()
        {
            BallThrow ball1 = new BallThrow();
            ball1.AddScore(10);
            _scoring.AddBall(ball1);

            BallThrow ball2 = new BallThrow();
            ball2.AddScore(10);
            _scoring.AddBall(ball2);

            BallThrow ball3 = new BallThrow();
            ball3.AddScore(10);
            _scoring.AddBall(ball3);

            BallThrow ball4 = new BallThrow();
            ball4.AddScore(10);
            _scoring.AddBall(ball4);

            BallThrow ball5 = new BallThrow();
            ball5.AddScore(10);
            _scoring.AddBall(ball5);

            BallThrow ball6 = new BallThrow();
            ball6.AddScore(10);
            _scoring.AddBall(ball6);

            BallThrow ball7 = new BallThrow();
            ball7.AddScore(10);
            _scoring.AddBall(ball7);

            BallThrow ball8 = new BallThrow();
            ball8.AddScore(10);
            _scoring.AddBall(ball8);

            BallThrow ball9 = new BallThrow();
            ball9.AddScore(10);
            _scoring.AddBall(ball9);

            BallThrow ball10 = new BallThrow();
            ball10.AddScore(10);
            _scoring.AddBall(ball10);

            BallThrow ball11 = new BallThrow();
            ball11.AddScore(10);
            _scoring.AddBall(ball11);

            BallThrow ball12 = new BallThrow();
            ball12.AddScore(10);
            _scoring.AddBall(ball12);

            Assert.AreEqual(300, _scoring.GetTotalScroe());
        }

        [Test]
        public void Test11score()
        {
            BallThrow ball1 = new BallThrow();
            ball1.AddScore(0);
            _scoring.AddBall(ball1);

            BallThrow ball2 = new BallThrow();
            ball2.AddScore(0);
            _scoring.AddBall(ball2);

            BallThrow ball3 = new BallThrow();
            ball3.AddScore(0);
            _scoring.AddBall(ball3);

            BallThrow ball4 = new BallThrow();
            ball4.AddScore(0);
            _scoring.AddBall(ball4);

            BallThrow ball5 = new BallThrow();
            ball5.AddScore(0);
            _scoring.AddBall(ball5);

            BallThrow ball6 = new BallThrow();
            ball6.AddScore(0);
            _scoring.AddBall(ball6);

            BallThrow ball7 = new BallThrow();
            ball7.AddScore(0);
            _scoring.AddBall(ball7);

            BallThrow ball8 = new BallThrow();
            ball8.AddScore(0);
            _scoring.AddBall(ball8);

            BallThrow ball9 = new BallThrow();
            ball9.AddScore(0);
            _scoring.AddBall(ball9);

            BallThrow ball10 = new BallThrow();
            ball10.AddScore(0);
            _scoring.AddBall(ball10);

            BallThrow ball11 = new BallThrow();
            ball11.AddScore(0);
            _scoring.AddBall(ball11);

            BallThrow ball12 = new BallThrow();
            ball12.AddScore(0);
            _scoring.AddBall(ball12);

            BallThrow ball13 = new BallThrow();
            ball13.AddScore(0);
            _scoring.AddBall(ball13);

            BallThrow ball14 = new BallThrow();
            ball14.AddScore(0);
            _scoring.AddBall(ball14);

            BallThrow ball15 = new BallThrow();
            ball15.AddScore(0);
            _scoring.AddBall(ball15);

            BallThrow ball16 = new BallThrow();
            ball16.AddScore(0);
            _scoring.AddBall(ball16);

            BallThrow ball17 = new BallThrow();
            ball17.AddScore(0);
            _scoring.AddBall(ball17);

            BallThrow ball18 = new BallThrow();
            ball18.AddScore(0);
            _scoring.AddBall(ball18);

            BallThrow ball19 = new BallThrow();
            ball19.AddScore(10);
            _scoring.AddBall(ball19);

            BallThrow ball20 = new BallThrow();
            ball20.AddScore(1);
            _scoring.AddBall(ball20);

            BallThrow ball21 = new BallThrow();
            ball21.AddScore(0);
            _scoring.AddBall(ball21);

            Assert.AreEqual(11, _scoring.GetTotalScroe());
        }

        [Test]
        public void Test12score()
        {
            BallThrow ball1 = new BallThrow();
            ball1.AddScore(0);
            _scoring.AddBall(ball1);

            BallThrow ball2 = new BallThrow();
            ball2.AddScore(0);
            _scoring.AddBall(ball2);

            BallThrow ball3 = new BallThrow();
            ball3.AddScore(0);
            _scoring.AddBall(ball3);

            BallThrow ball4 = new BallThrow();
            ball4.AddScore(0);
            _scoring.AddBall(ball4);

            BallThrow ball5 = new BallThrow();
            ball5.AddScore(0);
            _scoring.AddBall(ball5);

            BallThrow ball6 = new BallThrow();
            ball6.AddScore(0);
            _scoring.AddBall(ball6);

            BallThrow ball7 = new BallThrow();
            ball7.AddScore(0);
            _scoring.AddBall(ball7);

            BallThrow ball8 = new BallThrow();
            ball8.AddScore(0);
            _scoring.AddBall(ball8);

            BallThrow ball9 = new BallThrow();
            ball9.AddScore(0);
            _scoring.AddBall(ball9);

            BallThrow ball10 = new BallThrow();
            ball10.AddScore(0);
            _scoring.AddBall(ball10);

            BallThrow ball11 = new BallThrow();
            ball11.AddScore(0);
            _scoring.AddBall(ball11);

            BallThrow ball12 = new BallThrow();
            ball12.AddScore(0);
            _scoring.AddBall(ball12);

            BallThrow ball13 = new BallThrow();
            ball13.AddScore(0);
            _scoring.AddBall(ball13);

            BallThrow ball14 = new BallThrow();
            ball14.AddScore(0);
            _scoring.AddBall(ball14);

            BallThrow ball15 = new BallThrow();
            ball15.AddScore(0);
            _scoring.AddBall(ball15);

            BallThrow ball16 = new BallThrow();
            ball16.AddScore(0);
            _scoring.AddBall(ball16);

            BallThrow ball17 = new BallThrow();
            ball17.AddScore(10);
            _scoring.AddBall(ball17);

            BallThrow ball18 = new BallThrow();
            ball18.AddScore(1);
            _scoring.AddBall(ball18);

            BallThrow ball19 = new BallThrow();
            ball19.AddScore(0);
            _scoring.AddBall(ball19);

            Assert.AreEqual(12, _scoring.GetTotalScroe());
        }

        [Test]
        public void Test158score()
        {
            BallThrow ball1 = new BallThrow();
            ball1.AddScore(0);
            _scoring.AddBall(ball1);

            BallThrow ball2 = new BallThrow();
            ball2.AddScore(0);
            _scoring.AddBall(ball2);

            BallThrow ball3 = new BallThrow();
            ball3.AddScore(4);
            _scoring.AddBall(ball3);

            BallThrow ball4 = new BallThrow();
            ball4.AddScore(6);
            _scoring.AddBall(ball4);

            BallThrow ball5 = new BallThrow();
            ball5.AddScore(10);
            _scoring.AddBall(ball5);

            BallThrow ball6 = new BallThrow();
            ball6.AddScore(9);
            _scoring.AddBall(ball6);

            BallThrow ball7 = new BallThrow();
            ball7.AddScore(0);
            _scoring.AddBall(ball7);

            BallThrow ball8 = new BallThrow();
            ball8.AddScore(10);
            _scoring.AddBall(ball8);

            BallThrow ball9 = new BallThrow();
            ball9.AddScore(2);
            _scoring.AddBall(ball9);

            BallThrow ball10 = new BallThrow();
            ball10.AddScore(3);
            _scoring.AddBall(ball10);

            BallThrow ball11 = new BallThrow();
            ball11.AddScore(10);
            _scoring.AddBall(ball11);

            BallThrow ball12 = new BallThrow();
            ball12.AddScore(7);
            _scoring.AddBall(ball12);

            BallThrow ball13 = new BallThrow();
            ball13.AddScore(3);
            _scoring.AddBall(ball13);

            BallThrow ball14 = new BallThrow();
            ball14.AddScore(10);
            _scoring.AddBall(ball14);

            BallThrow ball15 = new BallThrow();
            ball15.AddScore(7);
            _scoring.AddBall(ball15);

            BallThrow ball16 = new BallThrow();
            ball16.AddScore(3);
            _scoring.AddBall(ball16);

            BallThrow ball17 = new BallThrow();
            ball17.AddScore(10);
            _scoring.AddBall(ball17);

            Assert.AreEqual(148, _scoring.GetTotalScroe());
        }
    }
}
