﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingGameScore.Bll.Classes
{
    public class Frame
    {
        public enum FrameTypes
        {
            Strike,
            Spare,
            OpenFrame,
            InProgress
        }

        const int bonusStrikeCount = 2;
        const int bonusSpareCount = 1;

        public FrameTypes frameType { get; set; }
        public int FrameNo { get; set; }
        public List<BallThrow> ballsScoreOfFrame { get; set; } = new List<BallThrow>();
        public List<BallThrow> bonusBall { get; set; } = new List<BallThrow>();

        #region [Methods]

        public void AddBall(BallThrow ballThrow)
        {
            ballsScoreOfFrame.Add(ballThrow);
            SetFrameType();
        }

        private void SetFrameType()
        {
            if (ballsScoreOfFrame.Count() == 2)
            {
                if (ballsScoreOfFrame.Select(b => b.score).Sum() == 10) 
                    frameType = FrameTypes.Spare;
                else 
                    frameType = FrameTypes.OpenFrame;
            }
            else if (ballsScoreOfFrame.Count() == 1)
            {
                if (ballsScoreOfFrame.Last().score == 10) 
                    frameType = FrameTypes.Strike;
                else 
                    frameType = FrameTypes.InProgress;
            }
        }

        public bool IsFameNeedBonusScore()
        {
            bool result = false;

            if (frameType == FrameTypes.Strike && bonusBall.Count() != bonusStrikeCount) 
                result = true;
            else if (frameType == FrameTypes.Spare && bonusBall.Count() != bonusSpareCount) 
                result = true;

            return result;
        }
        #endregion
    }
}
