﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingGameScore.Bll.Classes
{
    public class Scoring
    {
        public List<Frame> frameKeep { get; set; } = new List<Frame>();
        private bool isFirstBallOfTheGame = true;
        private const int possibleToAddBonusFrameCount = 2;

        #region [Methods]

        public void AddBall(BallThrow ballThrow)
        {
            if (isFirstBallOfTheGame)
            {
                Frame frame = new Frame();
                frame.AddBall(ballThrow);
                frame.FrameNo = 1;
                frameKeep.Add(frame);
                isFirstBallOfTheGame = false;
            }
            else
            {
                assignBonusBallToNotCompleteFrames(ballThrow, frameKeep.Count - 1, 0);

                if (frameKeep.Last().frameType == Frame.FrameTypes.InProgress)
                    frameKeep.Last().AddBall(ballThrow);
                else
                {
                    Frame frame = new Frame();
                    frame.AddBall(ballThrow);
                    frame.FrameNo = frameKeep.Count() + 1;
                    frameKeep.Add(frame);
                }
            }
        }

        private void assignBonusBallToNotCompleteFrames(BallThrow ballThrow, int indexOfFrameKeep, int passedFrameCount)
        {
            if (passedFrameCount >= possibleToAddBonusFrameCount || indexOfFrameKeep < 0)
                return;

            var previousFrame = frameKeep[indexOfFrameKeep];

            if (previousFrame.IsFameNeedBonusScore() && previousFrame.FrameNo < 10)
            {
                previousFrame.bonusBall.Add(ballThrow);
            }
            passedFrameCount++;

            assignBonusBallToNotCompleteFrames(ballThrow, indexOfFrameKeep - 1, passedFrameCount);
        }

        public int GetTotalScroe()
        {
            int totalScore = 0;
            totalScore = frameKeep.Any() ? frameKeep.SelectMany(f => f.ballsScoreOfFrame).Select(b => b.score).Sum()
                                            + frameKeep.SelectMany(f => f.bonusBall).Select(b => b.score).Sum()
                                         : 0;
            return totalScore;
        }
        #endregion
    }
}
