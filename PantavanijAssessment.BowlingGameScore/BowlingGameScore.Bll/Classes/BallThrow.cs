﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingGameScore.Bll.Classes
{
    public class BallThrow
    {
        public int score { get; set; }
        private bool isAddedScore = false;

        #region [Methods]

        public void AddScore(int score)
        {
            if (isAddedScore) 
                return;
            if (score < 0) 
                score = 0;
            if (score > 10) 
                score = 10;

            this.score = score;
            isAddedScore = true;
        }
        #endregion
    }
}
