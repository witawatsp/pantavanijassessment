﻿using BowlingGameScore.Bll.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingGameScore.Bll
{
    public static class BowlingScoreManager
    {
        public static int CalculateBowlingScorePerGame(IEnumerable<int> scoreThisGame)
        {
            int result = 0;
            Scoring score = new Scoring();
            foreach(int ballThrow in scoreThisGame)
            {
                score.AddBall(new BallThrow() { score = ballThrow});
            }
            result = score.GetTotalScroe();

            return result;
        }
    }
}
