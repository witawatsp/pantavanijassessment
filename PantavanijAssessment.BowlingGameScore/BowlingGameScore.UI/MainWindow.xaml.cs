﻿using BowlingGameScore.Bll;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BowlingGameScore.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new Microsoft.Win32.OpenFileDialog();
            var myTextBlock = (TextBox)this.FindName("OutputText");
            string result = "";

            IEnumerable<string> scoreAllGameAsText;
            List<int> bowlingScoreCalculatedList = new List<int>();

            try
            {
                if (dialog.ShowDialog() == true)
                {
                    var fullPath = dialog.FileName;
                    scoreAllGameAsText = File.ReadLines(fullPath);
                    foreach (var scoreThisGameAsText in scoreAllGameAsText)
                    {
                        IEnumerable<int> scoreThisGameAsInt = scoreThisGameAsText.Split(',').Select(t => Convert.ToInt32(t.Trim()));
                        bowlingScoreCalculatedList.Add(BowlingScoreManager.CalculateBowlingScorePerGame(scoreThisGameAsInt));
                    }
                    result = string.Join(Environment.NewLine, bowlingScoreCalculatedList);
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            myTextBlock.Text = result;
        }
    }
}
